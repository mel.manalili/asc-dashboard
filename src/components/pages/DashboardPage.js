import React from 'react' ;
import AdminCardSection1 from './sections/AdminCardSection1';
import BreadcrumSection from './sections/BreadcrumSection';
import DatePicker from './sections/DatePicker';
import ChartSection1 from './sections/ChartSection1';
import ChartSection2 from './sections/ChartSection2';

const DashboardPage =  () => {
  return (
    <React.Fragment>
      <DatePicker/>
      <BreadcrumSection />
      <AdminCardSection1 />
      <ChartSection1 />
      <ChartSection2 />

    </React.Fragment>
  )
}

export default DashboardPage;