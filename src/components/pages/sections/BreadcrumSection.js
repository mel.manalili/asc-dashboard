import React from 'react';
import { MDBCard, MDBCardBody, NavLink, MDBBreadcrumb, MDBBreadcrumbItem, MDBFormInline, MDBBtn } from 'mdbreact';

const BreadcrumSection = () => {
  return (
    <MDBCard className="mb-4">
        <MDBCardBody id="breadcrumb" className="d-flex align-items-center justify-content-between">
            <MDBBreadcrumb>
                <MDBBreadcrumbItem>
                  <a href="/dashboard">Home</a>
                  </MDBBreadcrumbItem>
                <MDBBreadcrumbItem>
                  <a href="/dashboard">Dashboard</a>
                </MDBBreadcrumbItem>
            </MDBBreadcrumb>
        </MDBCardBody>
    </MDBCard>
  )
}

export default BreadcrumSection;

