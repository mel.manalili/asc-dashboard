import React, { Component } from 'react';
import { MDBCol, MDBCard, MDBCardBody, MDBRow, MDBTable,MDBTableHead,MDBTableBody    } from 'mdbreact';
import { Bar} from 'react-chartjs-2';

class ChartSection1 extends Component {
    render(){
        const dataBar = {
            labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4'],
            datasets: [
            {
                label: 'Compliant',
                data: [12, 39, 3, 50, 2, 32, 84],
                backgroundColor: 'rgba(77, 5, 232, 1)',
                borderWidth: 1
            }, {
                label: 'Non-Compliant',
                data: [56, 24, 5, 16, 45, 24, 8],
                backgroundColor: 'rgba(248, 148, 6, 1)',
                borderWidth: 1
            }, 
            ]
        };
        
        const barChartOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
            xAxes: [{
                barPercentage: 1,
                gridLines: {
                display: true,
                color: 'rgba(0, 0, 0, 0.1)'
                }
            }],
            yAxes: [{
                gridLines: {
                display: true,
                color: 'rgba(0, 0, 0, 0.1)'
                },
                ticks: {
                beginAtZero: true
                }
            }]
            }
        }

        return (
            <MDBRow className="mb-4">
                <MDBCol md="6"className="mb-4">
                    <MDBCard className="mb-4">
                        <MDBCardBody>
                          Monitoring Result
                          <MDBCardBody>
                          <Bar data={dataBar} height={216.5} options={barChartOptions} />
                          </MDBCardBody>
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>

                <MDBCol md="6" className="mb-4">
            <MDBCard>
                <MDBCardBody>
                <p class="text-dark">Top Violators by Brand</p>
                  <MDBTable hover>
                    <MDBTableHead color="orange">
                      <tr>
                        <th>Rank</th>
                        <th>Brand</th>
                        <th>Company</th>
                        <th>Total</th>
                      </tr>
                    </MDBTableHead>
                    <MDBTableBody>
                      <tr>
                        <td>1</td>
                        <td>Milo</td>
                        <td>Nestle</td>
                        <td>51</td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Nido</td>
                        <td>Nestle</td>
                        <td>38</td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Alaxan</td>
                        <td>Unilab</td>
                        <td>14</td>
                      </tr>
                    </MDBTableBody>
                  </MDBTable>
                </MDBCardBody>
            </MDBCard>
        </MDBCol>



            </MDBRow>


            
        )

        
    }
}

export default ChartSection1;

