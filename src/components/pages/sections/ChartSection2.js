import React, { Component } from 'react';
import { MDBCol, MDBCard, MDBCardBody, MDBRow, MDBTable,MDBTableHead,MDBTableBody    } from 'mdbreact';
import { HorizontalBar} from 'react-chartjs-2';

class ChartSection2 extends Component {
    render(){
        
        const data = {
          labels: ['Medical Supply', 'Telecommunication', 'Drinks', 'Milk'],
          datasets: [
            {
              label: 'Commercial Category',
              backgroundColor: 'rgba(248, 148, 6, 1)',
              borderColor: 'rgba(248, 148, 6, 1)',
              borderWidth: 1,
              hoverBackgroundColor: 'rgba(248, 148, 6, 1)',
              hoverBorderColor: 'rgba(248, 148, 6, 1)',
              data: [65, 59, 80, 81, 56, 55, 40]
            }
          ]
        }

        return (
            <MDBRow className="mb-4">
                <MDBCol md="6"className="mb-4">
                    <MDBCard className="mb-4">
                        <MDBCardBody>
                          Top Violations by Commercial Category
                          <MDBCardBody>
                            <HorizontalBar data={data} height={135} />
                          </MDBCardBody>
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>

                <MDBCol md="6" className="mb-4">
            <MDBCard>
                <MDBCardBody>
                <p class="text-dark">Top Violations</p>
                <MDBTable hover>
                      <MDBTableHead color="orange">
                        <tr>
                          <th>Rank</th>
                          <th>Violations</th>
                          <th>Total</th>
                        </tr>
                      </MDBTableHead>
                      <MDBTableBody>
                        <tr>
                          <td>1</td>
                          <td>Tempo</td>
                          <td>111</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Duration</td>
                          <td>75</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>Quality</td>
                          <td>10</td>
                        </tr>
                      </MDBTableBody>
                    </MDBTable>
                </MDBCardBody>
            </MDBCard>
        </MDBCol>



            </MDBRow>


            
        )

        
    }
}

export default ChartSection2;

