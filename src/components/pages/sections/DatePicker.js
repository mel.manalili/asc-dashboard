import React from 'react';
import { MDBCard, MDBCardBody, MDBIcon} from 'mdbreact';

const DatePicker = () => {
  return(
    
  <MDBCard className="mb-3">
    <MDBCardBody id="calendar" className="d-flex justify-content-end" >
      <MDBIcon icon="far fa-calendar-alt" className="mr-3" />
      <div id="daterange" class="selectbox pull-right">
			<i class="fa fa-calendar"></i>
			<span>January 8, 2020 - February 6, 2020</span> <b class="caret"></b>
		</div>

      <MDBIcon icon="fa fa-caret-down" className="mr-3"/>   
    </MDBCardBody>  
   </MDBCard>
  )
}

export default DatePicker;

