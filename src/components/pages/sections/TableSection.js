import React from 'react';
import { MDBCard, MDBCardBody, MDBTable, MDBTableBody, MDBTableHead, MDBRow, MDBCol } from 'mdbreact';

const TableSection = () => {
  return (

    <MDBRow className="mb-4">
          <MDBCol md="6">
              <MDBCard>
                  <MDBCardBody>
   

                  </MDBCardBody>
              </MDBCard>
          </MDBCol>


          
          <MDBCol md="6" className="mb-4">
              <MDBCard>
                  <MDBCardBody>
                  <p class="text-dark">Top Violators by Brand</p>
                    <MDBTable hover>
                      <MDBTableHead color="orange">
                        <tr>
                          <th>Rank</th>
                          <th>Brand</th>
                          <th>Company</th>
                          <th>Total</th>
                        </tr>
                      </MDBTableHead>
                      <MDBTableBody>
                        <tr>
                          <td>1</td>
                          <td>Milo</td>
                          <td>Nestle</td>
                          <td>51</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Nido</td>
                          <td>Nestle</td>
                          <td>38</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>Alaxan</td>
                          <td>Unilab</td>
                          <td>14</td>
                        </tr>
                      </MDBTableBody>
                    </MDBTable>
                  </MDBCardBody>
              </MDBCard>
          </MDBCol>


          <MDBCol md="6">
              <MDBCard>
                  <MDBCardBody>
   

                  </MDBCardBody>
              </MDBCard>
          </MDBCol>


          <MDBCol md="6" className="mb-4">
              <MDBCard>
                  <MDBCardBody>
                  <p class="text-dark">Top Violations</p>
                     
                     <MDBTable hover>
                      <MDBTableHead color="orange">
                        <tr>
                          <th>Rank</th>
                          <th>Violations</th>
                          <th>Total</th>
                        </tr>
                      </MDBTableHead>
                      <MDBTableBody>
                        <tr>
                          <td>1</td>
                          <td>Tempo</td>
                          <td>111</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Duration</td>
                          <td>75</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>Quality</td>
                          <td>10</td>
                        </tr>
                      </MDBTableBody>
                    </MDBTable>
                  </MDBCardBody>
              </MDBCard>
          </MDBCol>

      </MDBRow>
  )
}

export default TableSection;

